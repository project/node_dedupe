Node Dedupe module disables users from creating duplicate nodes by re-submitting node forms using the browser's back button.

Install and enable the module as usual, then visit the module's settings page at - admin/settings/node_dedupe. 

On the settings page you may select the node types you wish to enable node deduping for and the error message to display to a user when submitting a duplicate node form.